var gulp = require('gulp');
var less = require('gulp-less');
var sync = require('browser-sync').create();
var mini = require('gulp-clean-css');
var path = require('path');
var maps = require('gulp-sourcemaps');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var inject = require('gulp-inject');
var gulpif = require('gulp-if');

var config = {
    "styles": {
        "src": ["src/assets/less/**/*.less","src/pages/**/*.less","!index.less"],
        "index":"src/assets/less/index.less",
        "dest": "dist/css",
        "bundle_name": "all.min.css",
        "minify": false,
        "create_source_map":true
    }
};

gulp.task('less', function(){
    return gulp.src(config.styles.index)
            .pipe(inject(gulp.src(config.styles.src, {read: false, cwd: ''}), {
                starttag: '/* inject:imports */',
                endtag: '/* endinject */',
                transform: function (filepath) {
                    return '@import ".' + filepath + '";';
                }
            }))
            .pipe(gulpif(config.styles.create_source_map, maps.init()))
            .pipe(less()).on('error', function(err){
                gutil.log(err);
                this.emit('end');
            })
            .pipe(mini())
            .pipe(concat(config.styles.bundle_name))
            .pipe(gulpif(config.styles.create_source_map, maps.write('./')))
            .pipe(gulp.dest(config.styles.dest))
            .pipe(sync.stream({match: '**/*.css'}));
});

gulp.task('html', function(){
    return gulp.src(['src/**/*.html'])
            .pipe(rename({dirname: ''}))
            .pipe(gulp.dest('dist'));
});

gulp.task('images', function(){
    return gulp.src(['src/assets/img/**/*.*'])
            .pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', function(){
    return gulp.src(['src/assets/fonts/**/*.*'])
            .pipe(gulp.dest('dist/fonts'));
});

gulp.task('js', function(){
    return gulp.src(
            [
                'bower_components/jquery/dist/jquery.min.js',
                path.join('src/**/', '*.js')
            ]
    )
            .pipe(concat('all.min.js'))
            .pipe(uglify().on('error', uglify.GulpUglifyError))
            .pipe(gulp.dest('dist/js'));
});

gulp.task('default', ['less', 'js', 'html', 'images', 'fonts'], function(){
    sync.init({
        server: "dist/"
    });

    gulp.watch("src/assets/img/**/*.*", ['images']);
    gulp.watch("src/assets/fonts/**/*.*", ['fonts']);
    gulp.watch("src/**/*.less", ['less']);
    gulp.watch("src/**/*.html", ['html']);
    gulp.watch("src/**/*.js",   ['js']);
    gulp.watch("dist/*.html").on('change', sync.reload);
    gulp.watch("dist/js/*.js").on('change', sync.reload);
});
